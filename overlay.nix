self: super: {
	git-minisign = super.callPackage ./pkgs/scripts/git-minisign {};

	# graphene-hardened-malloc = super.callPackage ./pkgs/development/libraries/graphene-hardened-malloc {};

	libcwtch-go = super.callPackage ./pkgs/development/libraries/libcwtch-go {};

	cwtch = super.callPackage ./pkgs/applications/communication/cwtch {zenity = super.gnome.zenity;};
}
