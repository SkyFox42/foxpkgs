{
	description = "A set of Nix expressions for packages either not available in Nixpkgs, outdated in Nixpkgs or written by me.";

	inputs = {
		nixpkgs.url = "github:nixos/nixpkgs/nixos-22.05";

		flake-utils.url = "github:numtide/flake-utils";
	};

	outputs = {self, nixpkgs, flake-utils}:
		flake-utils.lib.eachDefaultSystem (system:
			let
				pkgs = import nixpkgs {
					inherit system;
				};

				cwtch-ui = pkgs.callPackage ./pkgs/applications/communication/cwtch {zenity = pkgs.gnome.zenity;};

			in {
				packages = flake-utils.lib.flattenTree {
					cwtch-ui = cwtch-ui;

					libcwtch-go = pkgs.callPackage ./pkgs/development/libraries/libcwtch-go {};

					git-minisign = pkgs.callPackage ./pkgs/scripts/git-minisign {};
				};

				apps = {
					cwtch-ui = flake-utils.lib.mkApp {drv = cwtch-ui; name = "cwtch";};
				};

				overlay = final: prev: {
					cwtch-ui = prev.callPackage ./pkgs/applications/communication/cwtch {zenity = prev.gnome.zenity;};

					libcwtch-go = prev.callPackage ./pkgs/development/libraries/libcwtch-go {};

					git-minisign = prev.callPackage ./pkgs/scripts/git-minisign {};
				};
			}
		);
}
