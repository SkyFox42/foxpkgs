{ stdenv, lib, fetchgit, git, minisign }:

stdenv.mkDerivation {
  pname = "git-minisign";
  version = "0.1";
  src = fetchgit {
    url = "https://codeberg.org/SkyFox42/git-minisign";
    rev = "ff328df1a6cc592386bb69546add2a219be9743f";
    sha256 = "1575bmn6pgd1yl3pci7swn5hhrdmb5jpbj5b55silr6p56in8c0q";
  };

  installPhase = ''
    mkdir -p $out/bin
    cp ./sh/git-minisign-sign.sh $out/bin/git-minisign-sign
    cp ./sh/git-minisign-verify.sh $out/bin/git-minisign-verify
  '';

  propagatedBuildInputs = [
    git
    minisign
  ];

  meta = with lib; {
    description = "Scripts to sign and verify Git commits using Minisign";
    longDescription = ''
      Scripts to sign and verify Git commits using Minisign
    '';
    homepage = "https://codeberg.org/SkyFox42/git-minisign";
    license = licenses.mit;
    maintainers = [
      {
        email = "skyfox42@protonmail.com";
        github = "SkyFox42";
        name = "SkyFox42";
      }
    ];
    platforms = platforms.unix;
  };
}
