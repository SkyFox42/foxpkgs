{
	lib
, stdenv
, fetchurl
, tor
, glibc
, gtk3
, pango
, harfbuzz
, atk
, cairo
, gdk-pixbuf
, glib
, gcc-unwrapped
, epoxy
, buildFHSUserEnv
, runCommand
, bash
, makeDesktopItem
, zenity
}:
let
	pname = "cwtch";
	version = "1.7.1";
	drvName = "${pname}-${version}";

	cwtch = stdenv.mkDerivation rec {
		name = "${drvName}-unwrapped";
		inherit version;

		src = fetchurl {
			url = "https://cwtch.im/releases/cwtch-v${if (lib.lists.last (lib.versions.splitVersion version)) == "0"
				then lib.versions.majorMinor version
				else version}/cwtch-v${version}.tar.gz";
			hash = "sha256-KYx0J74b/MGFCzRCNPWIhcHjVh8kAAWQMd0o7KmMDmw=";
		};

		buildInputs = [
			tor
			zenity
		];

		dontCheck = true;
		dontBuild = true;
		installPhase = ''
			mkdir -vp $out/bin
			mkdir -vp $out/lib/cwtch
			mkdir -vp $out/share/{icons,cwtch}
			mkdir -vp $out/share/icons/hicolor/512x512/apps
			cp -v ./lib/cwtch $out/bin/
			cp -v cwtch.png $out/share/icons/hicolor/512x512/apps
			cp -v cwtch.png $out/share/icons
			cp -rv ./data $out/share/cwtch
			cp -v ./lib/libapp.so $out/lib/cwtch
			cp -v ./lib/*.so $out/lib
			chmod +x $out/bin/cwtch
		'';
	};

	fhsEnv = buildFHSUserEnv {
		name = "${drvName}-fhs-env";
		targetPkgs = pkgs:
			with pkgs; [
				tor
				glibc
				gtk3
				pango.out
				harfbuzz
				atk
				cairo
				gdk-pixbuf
				glib.out
				gcc-unwrapped.lib
				epoxy
				cwtch
				zenity
			];
		runScript = "${cwtch}/bin/cwtch";
	};

	desktopItem = makeDesktopItem {
		name = drvName;
		exec = "cwtch";
		icon = "${cwtch}/share/icons/hicolor/512x512/apps/cwtch.png";
		comment = "Metadata Resistant Chat";
		desktopName = "Cwtch";
		genericName = "Instant Messaging";
		categories = ["Network" "InstantMessaging"];
	};

in runCommand drvName {
	startScript = ''
		#!${bash}/bin/bash
		${fhsEnv}/bin/${drvName}-fhs-env
	'';
	preferLocalBuild = true;
	allowSubstitutes = false;
	passthru = { unwrapped = cwtch; };

	meta = with lib; {
		homepage = "https://cwtch.im/";
		description = "A Flutter based Cwtch UI https://cwtch.im";
		longDescription = ''
			This is a decentralized, privacy-preserving, multi-party messaging protocol
			that can be used to build metadata resistant applications.
		'';
		license = licenses.mit;
		maintainers = with maintainers; [
			{
				email = "skyfox42@protonmail.com";
				github = "SkyFox42";
				name = "SkyFox42";
			}
		];
		platforms = [ "x86_64-linux" ];
	};
} ''
	mkdir -pv $out/bin
	mkdir -pv $out/share/applications

	echo -n "$startScript" > $out/bin/${pname}
	cp -av ${desktopItem}/share/applications/* $out/share/applications
	chmod +x $out/bin/${pname}
''
