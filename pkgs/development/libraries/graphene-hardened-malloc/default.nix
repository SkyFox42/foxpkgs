# Pretty much the same as the one in Nixpkgs
# https://github.com/NixOS/nixpkgs/blob/master/pkgs/development/libraries/graphene-hardened-malloc/default.nix
# Just updated the version
{ stdenv, lib, fetchurl }:

stdenv.mkDerivation rec {
  pname = "graphene-hardened-malloc";
  version = "7";

  src = fetchurl {
    url = "https://github.com/GrapheneOS/hardened_malloc/archive/refs/tags/${version}.tar.gz";
    sha256 = "12z48bpd25k5bs61f9jlka3sgqasc864mxrmz8dngxkdk0s0x9px";
  };

  installPhase = ''
    install -Dm444 -t $out/lib libhardened_malloc.so
    mkdir -p $out/bin
    substitute preload.sh $out/bin/preload-hardened-malloc --replace "\$dir" $out/lib
    chmod 0555 $out/bin/preload-hardened-malloc
  '';

  separateDebugInfo = true;

  doInstallCheck = true;
  installCheckPhase = ''
    pushd test
    make
    $out/bin/preload-hardened-malloc ./offset
    pushd simple-memory-corruption
    make
    # these tests don't actually appear to generate overflows currently
    rm read_after_free_small string_overflow
    # eight_byte_overflow_large
    # these tests do not seem to abort currently
    rm malloc_object_size malloc_object_size_offset
    for t in `find . -regex ".*/[a-z_]+"` ; do
      echo "Running $t..."
      # the program being aborted (as it should be) would result in an exit code > 128
      (($out/bin/preload-hardened-malloc $t) && false) \
        || (test $? -gt 128 || (echo "$t was not aborted" && false))
    done
    popd
    popd
  '';

  meta = with lib; {
    homepage = "https://github.com/GrapheneOS/hardened_malloc";
    description = "Hardened allocator designed for modern systems";
    longDescription = ''
      This is a security-focused general purpose memory allocator providing the malloc API
      along with various extensions. It provides substantial hardening against heap
      corruption vulnerabilities yet aims to provide decent overall performance.
    '';
    license = licenses.mit;
    maintainers = with maintainers; [
      ris
      {
        email = "skyfox42@protonmail.com";
        github = "SkyFox42";
        name = "SkyFox42";
      }
    ];
    platforms = [ "x86_64-linux" "aarch64-linux" ];
  };
}
