{ lib, buildGoModule }:

buildGoModule rec {
	pname = "libcwtch-go";
	version = "1.7.1";

	src = builtins.fetchGit {
		url = "https://git.openprivacy.ca/cwtch.im/libcwtch-go";
		ref = "refs/tags/${version}";
		rev = "02c7b0b962b2dbf5ccd474e4d4198b1c2dedb450";
	};

	buildPhase = ''
		./switch-ffi.sh
		go build -buildmode c-shared -o libCwtch.so
	'';

	vendorSha256 = "sha256-kMzxiR0kjZEGnMhuWu1keNlxRQD6Gek+p7e5HOKH308=";

	installPhase = ''
		mkdir -p $out/include
		mkdir $out/lib
		cp libCwtch.h $out/include
		cp libCwtch.so $out/lib
	'';

	meta = with lib; {
		homepage = "https://git.openprivacy.ca/cwtch.im/libcwtch-go";
		description = "C-bindings for the Go Cwtch Library https://cwtch.im";
		longDescription = ''
			This is a decentralized, privacy-preserving, multi-party messaging protocol
			that can be used to build metadata resistant applications.
		'';
		license = licenses.mit;
		maintainers = with maintainers; [
			{
				email = "skyfox42@protonmail.com";
				github = "SkyFox42";
				name = "SkyFox42";
			}
		];
		platforms = [ "x86_64-linux" ];
	};
}
