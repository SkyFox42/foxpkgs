# Foxpkgs

A set of [Nix](https://nixos.org/nix/) expressions for packages either not available in [Nixpkgs](https://github.com/NixOS/nixpkgs), outdated in Nixpkgs or written by me.

## License

Foxpkgs is licensed under the MIT license.

Note: MIT license does not apply to the packages defined by Foxpkgs, merely to the files in this repository (the Nix expressions, build scripts, NixOS modules, etc.). It also might not apply to patches included in Foxpkgs, which may be derivative works of the packages to which they apply. The aforementioned artifacts are all covered by the licenses of the respective packages.

P.S. the license section is taken from the [Nixpkgs repo](https://github.com/NixOS/nixpkgs) :)
