{ pkgs ? import <nixpkgs> { } }:

rec {
	git-minisign = pkgs.callPackage ./pkgs/scripts/git-minisign {};

	# graphene-hardened-malloc = pkgs.callPackage ./pkgs/development/libraries/graphene-hardened-malloc {};
	graphene-hardened-malloc = pkgs.graphene-hardened-malloc;

	libcwtch-go = pkgs.callPackage ./pkgs/development/libraries/libcwtch-go {};

	cwtch = pkgs.callPackage ./pkgs/applications/communication/cwtch {zenity = pkgs.gnome.zenity;};
}
